//using Microsoft.Win32;
using System;
using System.Diagnostics;
using System.Drawing;
using System.Drawing.Imaging;
//using System.Drawing.Imaging;
using System.IO;
using System.Media;
using System.Net;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Security.Cryptography;
using System.Threading;
using System.Windows.Forms;
//using wallpaper;
namespace Screenshotter
{
    internal static class Program

    {

        public static void Main(string[] args)
        {
            Process[] pname = Process.GetProcessesByName("NeedScreenUpdater");
            if (pname.Length == 0)
            {
                MessageBox.Show("Uruchom NeedScreen poprzez NeedScreenUpdater!");
                Process.GetCurrentProcess().Kill();
            }



            string md5_string = "";
            string remote_string = "";
            using (var md5 = MD5.Create())
            {
                using (var stream = File.OpenRead(@"C:\NeedScreen\NeedScreenUpdater.exe"))
                {
                    byte[] md5_result = md5.ComputeHash(stream);
                    md5_string = BitConverter.ToString(md5_result).Replace("-", "").ToLower();
                    var remote_result = "394F3127F773F0227A20A332858E9222";
                    remote_result = remote_result.Replace(" ", "").ToLower();
                    string exeDir = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);
                    Directory.SetCurrentDirectory(exeDir);
                    stream.Close();

                    if (md5_string == remote_result)
                    {
                    }
                    else
                    {
                        foreach (var process in Process.GetProcessesByName("NeedScreenUpdater"))
                        {
                            process.Kill();
                        }
                        Thread.Sleep(1000);
                        Directory.SetCurrentDirectory(@"C:\NeedScreen");
                        if (File.Exists("NeedScreenUpdater.exe"))
                        {
                            File.Delete("NeedScreenUpdater.exe");
                        }
                        WebClient webClient = new WebClient();
                        webClient.DownloadFile("https://needscreen.needrp.net/NeedScreenUpdater.exe", @"C:\NeedScreen\NeedScreenUpdater.exe");
                        Process NeedUpdater = new Process();
                        NeedUpdater.StartInfo.FileName = @"C:\NeedScreen\NeedScreenUpdater.exe";
                        NeedUpdater.Start();
                        Process.GetCurrentProcess().Kill();

                    }


                }
            }



            if (args == null)
            {
                throw new ArgumentNullException(nameof(args));
            }
            if (File.Exists("screenshotcompressed.jpg"))
            {
                File.Delete("screenshotcompressed.jpg");
            }

            //string pcname = Environment.MachineName;
            //string pablo = "DESKTOP-4SFNHKK";
            //if (pablo == pcname)
            //{
            //    Wall o = new Wall();
            //    o.Main2();
            //}
            Thread screenshot = new Thread(ScreenShot);
            //Thread autoscreenshot = new Thread(AutoScreenShot);
            screenshot.SetApartmentState(ApartmentState.STA);
            screenshot.Start();
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            NotifyIcon notifyIcon1 = new NotifyIcon();
            ContextMenu contextMenu1 = new ContextMenu();
            MenuItem menuItem1 = new MenuItem();
            MenuItem menuItem2 = new MenuItem();
            MenuItem menuItem3 = new MenuItem();
            MenuItem menuItem4 = new MenuItem();
            MenuItem menuItem5 = new MenuItem();
            //MenuItem menuItem5 = new MenuItem();
            MenuItem menuItem6 = new MenuItem();
            contextMenu1.MenuItems.AddRange(new MenuItem[] {
                menuItem1
            });
            contextMenu1.MenuItems.AddRange(new MenuItem[] {
                menuItem2
            });
            contextMenu1.MenuItems.AddRange(new MenuItem[] {
                menuItem3
            });
            contextMenu1.MenuItems.AddRange(new MenuItem[] {
                menuItem4
            });
            contextMenu1.MenuItems.AddRange(new MenuItem[] {
                menuItem5
            });
            contextMenu1.MenuItems.AddRange(new MenuItem[] {
                menuItem6
            });
            //menuItem5.Index = 4;
            //menuItem5.Text = ">>> Wyślij dotację <<<";
            //menuItem5.Click += new EventHandler(MenuItem5_Click);
            menuItem1.Index = 1;
            menuItem1.Text = "Informacje";
            menuItem1.Click += new EventHandler(MenuItem1_Click);
            menuItem2.Index = 5;
            menuItem2.Text = "Zamknij";
            menuItem2.Click += new EventHandler(MenuItem2_Click);
            menuItem5.Index = 4;
            menuItem5.Text = "Włącz kompresje screenshotów";
            menuItem5.Click += new EventHandler(MenuItem5_Click);
            menuItem6.Index = 2;
            menuItem6.Text = "Dziennik zmian";
            menuItem6.Click += new EventHandler(MenuItem6_Click);
            menuItem3.Index = 3;
            menuItem3.Text = "Zrestartuj program";
            menuItem3.Click += new EventHandler(MenuItem3_Click);
            WebClient client4 = new WebClient();
            menuItem4.Index = 0;
            string productVersion = FileVersionInfo.GetVersionInfo(Assembly.GetExecutingAssembly().Location).ProductVersion;
            menuItem4.Text = "Wersja:" + " " + productVersion;
            menuItem4.Click += new EventHandler(MenuItem4_Click);
            menuItem4.Enabled = false;
            notifyIcon1.Icon = Screenshotter.Properties.Resources.ikonka;
            notifyIcon1.Text = "NeedScreen";
            notifyIcon1.ContextMenu = contextMenu1;
            notifyIcon1.Visible = true;
            WebClient client3 = new WebClient();
            string userName = System.Security.Principal.WindowsIdentity.GetCurrent().Name;
            string content2 = client3.DownloadString("http://hosting.needrp.net:8115/users.php?data=" + userName);

            if (File.Exists(@"C:\NeedScreen\compression.txt"))
            {
                string content = File.ReadAllText(@"C:\NeedScreen\compression.txt");
                if (content == "true")
                {
                    string compress = "true";
                    menuItem5.Checked = true;

                }
                else
                {
                    string compress = "false";
                    menuItem5.Checked = false;
                }
            }
            else
            {
                File.WriteAllText(@"C:\NeedScreen\compression.txt", "false");
            }

            Application.Run();
        }

        private static void MenuItem5_set(object sender, EventArgs e)
        {
        }
        private static void MenuItem5_Click(object sender, EventArgs e)
        {
            //set menuitem5 checkbox to true
            var menuItem = sender as MenuItem;
            if (menuItem.Checked == true)
            {
                menuItem.Checked = false;
                string compress = "false";
                File.WriteAllText(@"C:\NeedScreen\compression.txt", "false");
            }
            else
            {
                menuItem.Checked = true;
                string compress = "true";
                File.WriteAllText(@"C:\NeedScreen\compression.txt", "true");
            }
        }

        private static void MenuItem6_Click(object sender, EventArgs e)
        {
            WebClient client4 = new WebClient();
            string changelog = client4.DownloadString("https://ci.appveyor.com/api/projects/bartixxx/screen/artifacts/changelog.txt");
            MessageBox.Show(changelog, "Dziennik zmian");
        }

        //public static void MenuItem5_Click(object sender, EventArgs e)
        //{
        //    menuItem5. = true;
        //}

        private static void MenuItem1_Click(object Sender, EventArgs e)
        {
            WebClient client5 = new WebClient();
            string informacje = client5.DownloadString("https://gitlab.com/Bartixxx32/screen/-/raw/master/info.txt");
            MessageBox.Show(informacje, "Informacje");

            {
                MciSendString("set cdaudio door open", null, 0, 0);
            }
            {
                MciSendString("set cdaudio door closed", null, 0, 0);
            }
        }

        private static void MenuItem2_Click(object Sender, EventArgs e)
        {
            foreach (Process process in Process.GetProcessesByName("NeedScreenUpdater"))
            {
                process.Kill();
                Thread.Sleep(5000);
                Process.GetCurrentProcess().Kill();
            }
        }

        private static void MenuItem3_Click(object Sender, EventArgs e)
        {
            foreach (Process process in Process.GetProcessesByName("NeedScreenUpdater"))
            {
                process.Kill();
                Thread.Sleep(5000);
                Process NeedScreen = new Process();
                NeedScreen.StartInfo.FileName = @"NeedScreenUpdater.exe";
                NeedScreen.Start();
                Process.GetCurrentProcess().Kill();
            }
        }

        private static void MenuItem4_Click(object Sender, EventArgs e)
        {
            foreach (Process process in Process.GetProcessesByName("NeedScreenUpdater"))
            {
                process.Kill();
                Thread.Sleep(5000);
                Process NeedScreen = new Process();
                NeedScreen.StartInfo.FileName = @"NeedScreenUpdater.exe";
                NeedScreen.Start();
                Process.GetCurrentProcess().Kill();
            }
        }

        private static void ScreenShot()
        {
            ImageCodecInfo myImageCodecInfo;
            Encoder myEncoder;
            EncoderParameter myEncoderParameter;
            EncoderParameters myEncoderParameters;
            while (true)
            {
                Thread.Sleep(50);
                if (System.Windows.Input.Keyboard.IsKeyDown(System.Windows.Input.Key.PrintScreen) == true)
                {
                    int screenLeft = SystemInformation.VirtualScreen.Left;
                    int screenTop = SystemInformation.VirtualScreen.Top;
                    int screenWidth = SystemInformation.VirtualScreen.Width;
                    int screenHeight = SystemInformation.VirtualScreen.Height;

                    Bitmap bmp = new Bitmap(screenWidth, screenHeight);
                    myImageCodecInfo = GetEncoderInfo("image/jpeg");
                    using (Graphics g = Graphics.FromImage(bmp))
                    {
                        g.CopyFromScreen(screenLeft, screenTop, 0, 0, bmp.Size);
                        string compress = File.ReadAllText(@"C:\NeedScreen\compression.txt");
                        if (compress == "false")
                        {
                            myImageCodecInfo = GetEncoderInfo("image/png");
                            myEncoder = Encoder.Quality;
                            myEncoderParameters = new EncoderParameters(1);
                            myEncoderParameter = new EncoderParameter(myEncoder, 100L);
                            myEncoderParameters.Param[0] = myEncoderParameter;
                            bmp.Save("screenshot.png");
                        }
                        else
                        {
                            myImageCodecInfo = GetEncoderInfo("image/jpeg");
                            myEncoder = Encoder.Quality;
                            myEncoderParameters = new EncoderParameters(1);
                            myEncoderParameter = new EncoderParameter(myEncoder, 80L);
                            myEncoderParameters.Param[0] = myEncoderParameter;
                            bmp.Save("screenshot.jpg", myImageCodecInfo, myEncoderParameters);
                        }


                        SoundPlayer audio = new SoundPlayer(Screenshotter.Properties.Resources.upload);
                        audio.Play();

                        try
                        {
                            WebClient client = new WebClient();
                            string content = File.ReadAllText(@"C:\NeedScreen\compression.txt");
                            if (content == "true")
                            {
                                byte[] response = client.UploadFile("http://hosting.needrp.net:8115/upload.php", "screenshot.jpg");
                                string s = client.Encoding.GetString(response);
                                Clipboard.SetText(s);
                                File.AppendAllText("screeny.txt", s + Environment.NewLine);
                                if (s == "Failed" || s == "Fuck off")
                                {
                                    return;
                                }
                            }
                            else
                            {
                                byte[] response = client.UploadFile("http://hosting.needrp.net:8115/upload.php", "screenshot.png");
                                string s = client.Encoding.GetString(response);
                                Clipboard.SetText(s);
                                File.AppendAllText("screeny.txt", s + Environment.NewLine);
                                if (s == "Failed" || s == "Fuck off")
                                {
                                    return;
                                }
                            }

                            if (File.Exists("screenshot.png"))
                            {
                                File.Delete("screenshot.png");
                            }
                            Thread.Sleep(1000);
                            SoundPlayer audio2 = new SoundPlayer(Screenshotter.Properties.Resources.complete);
                            audio2.Play();
                        }
                        catch
                        {
                        }
                    }
                    Thread.Sleep(500);
                    bmp.Dispose();
                    Thread.Sleep(500);
                }
            }
        }

        private static ImageCodecInfo GetEncoderInfo(String mimeType)
        {
            int j;
            ImageCodecInfo[] encoders;
            encoders = ImageCodecInfo.GetImageEncoders();
            for (j = 0; j < encoders.Length; ++j)
            {
                if (encoders[j].MimeType == mimeType)
                    return encoders[j];
            }
            return null;
        }




        [DllImport("winmm.dll", EntryPoint = "mciSendString")]
        public static extern int MciSendString(string lpstrCommand, string lpstrReturnString, int uReturnLength, int hwndCallback);
    }
}